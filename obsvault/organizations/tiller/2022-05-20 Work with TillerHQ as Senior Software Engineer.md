



> *I was immediately extremely productive as measured by code check-ins, unit test coverage, and especially substantial new features and key partner integrations.*
> 
> *-- [Tim Johns, Head of Engineering at Tiller](https://www.linkedin.com/in/timjohnswa/details/experience/)*




