---
created: 2022-05-16T11:11:27-06:00
updated: 2022-05-16T11:11:27-06:00
---
# Obsidian



## Features

*Features added as I use/need to look up docs for them.*

[[Obsidian - Aliases]]

## Vault
> *Editor's Note: Section is here to link to, e.g. in [[Tools]].*
> *-- @gsteve3 2022-05-16 at 11:11:34*